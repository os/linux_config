# Linux Config
This repository describes how to set up a full, graphical virtual machine for class usage.

## Installing and configuring your VM

### Step 0
Actually read this and its sub-links, word for word!

<https://cs-mst.gitlab.io/index/ClassGeneral/WorkingEnvironment.html>

If you need a review of Git, then read this:

<https://cs-mst.gitlab.io/index/Classes/CompSciTools/Content/VersionControl.html>

### Step 1
Install a virtual machine manager (VMM).
If you are on Windows or an Intel/x86 Mac,
then use <https://www.virtualbox.org/> as your VMM.
If you are on an ARM M1 Mac, then you might try UTM, Parallels, or VMWare.
If you are on Linux, then you can choose skip the VM entirely on many assignments.
On Linux, KVM + virt-manager are nice for a VMM setup.

### Step 2
Download Fedora XFCE iso file:

<https://fedoraproject.org/spins/xfce/download>

Or, if you prefer another desktop manager, or run an non-Intel machine:

<https://fedoraproject.org/workstation/download>

### Step 3
Start your VM manager (VirtualBox itself).
Create a new VM with about 40-50gb of hard drive space.
In the VirtualBox settings, increase it's processors, RAM, and video memory to the max of the "green" sliders.
When you boot the VM for the first time, 
point it to the Fedora ISO you downloaded previously.
Once the ISO is booted into the live Linux, install it to the virtual disk!!
To do this, you have to actually lauch the installer.
After completing the installation, in the settings for the VM itself,
remove the ISO file from the virtual CDROM, reboot into Fedora Linux!

IMPORTANT:
**Verify you are NOT running in the liveCD,
but are actually booted into an installed VM.**
Open a terminal, and if your user has "live" in the name,
you did not follow the above steps!

Then, you can install all the needed software.
To do this, run the bash script present in this repository,
by opening a terminal in the VM, and running:
```
bash full_vm_config.sh
```
After that, in the VirtualBox settings,
snapshot your clean, un-broken VM,
so you can always go back to a reliable state!

## Installation scripts for a full graphical Fedora installation.
After installing your full Fedora virtual machine,
you can do post-configuration with this script:
`./full_vm_config.sh`
It includes a variety of software you might want,
including IDE's like Spyder, QtCreator, etc.

## Some dotfiles
You are not required to use these, but you may be curious.
`./dotfiles/` contains some of my dotfiles.
They are hidden files, with filenames starting with `.` so they are not visible by default.

`./dotfiles/.vim/` would have Vim plugns. 
The deepest directories are the plugns, which I did not include, but included their README files, 
so you can see where to put the reposity, for vim to see them.
