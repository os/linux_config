" TODO shift-tab does not work in st but it does in gnome term?
" https://vim.fandom.com/wiki/Make_Shift-Tab_work
"https://www.reddit.com/r/vim/comments/gahih3/suckless_terminal_and_shift_tab/
set t_kB=[Z

set tabstop=4
set expandtab
set shiftwidth=4
set autoindent
set encoding=utf-8
set fileencoding=utf-8
"set foldmethod=syntax
set number
set mouse=
set incsearch
set hlsearch
set lbr
set wrap
set diffopt+=followwrap
" nvim's alternative vim.opt.diffopt:append('followwrap')
" Nice for gx in active buffer
set autochdir

" powerline top tab line
set showtabline=2
" powerline bottom line
set laststatus=2
let g:lightline = {
      \ 'tabline': {
      \   'left': [ ['buffers'] ],
      \   'right': [ ['close'] ]
      \ },
      \ 'component_expand': {
      \   'buffers': 'lightline#bufferline#buffers'
      \ },
      \ 'component_type': {
      \   'buffers': 'tabsel'
      \ }
      \ }

"for nerdtree and buffers
let mapleader=","
"let maplocalleader=","

" tagbar
nmap <leader>tb :TagbarToggle<cr>
" nmap <leader>tb :Vista<cr>
let g:tagbar_left = 1

let g:vista_sidebar_position = 'vertical topleft'

nmap <leader>nt :NERDTreeFocus<cr>

" For buffer-tab operation
set hidden
nmap <leader>l :bn<cr>
nmap <leader>h :bp<cr>
nmap <leader>b :ls<cr>
nmap <leader>x :bd<cr>

" copy-paste to system clipboard
"https://stackoverflow.com/questions/30691466/what-is-difference-between-vims-clipboard-unnamed-and-unnamedplus-settings
"set clipboard=unnamedplus
set clipboard^=unnamed,unnamedplus

" set cursorline
" set spell
" set nospell

set nocompatible
filetype plugin on
syntax on
set background=dark
:colorscheme desert
" :colorscheme default
" :colorscheme desert

" makes diff mode wrap (actually better without it)
"au VimEnter * if &diff | execute 'windo set wrap' | endif

" diff mode colors sucked in general
if &diff
    highlight DiffAdd    ctermbg=237
    highlight DiffDelete ctermbg=237
    " DiffChange should not really be highlighted
    highlight DiffChange ctermbg=233
    highlight DiffText   ctermbg=237
endif

let g:syntastic_vim_checkers = ['vint']

"let g:pandoc#filetypes#handled = ["pandoc", "markdown"]

" Ok for privacy maybe?
" This could be handy...
" save undo trees in files
"set undofile
"set undodir=~/.vim/undo
" number of undo saved
"set undolevels=10000 
"nnoremap <F5> :MundoToggle<CR>
