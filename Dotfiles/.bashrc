# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

# Uncomment the following line if you don't like systemctl's auto-paging feature:
# export SYSTEMD_PAGER=

# User specific aliases and functions

#alias l.='ls -dF .* --color=auto --group-directories-first'
alias l.='eza -1d1FhH .* --group-directories-first'
#alias ll='ls -lFh --color=auto --group-directories-first'
alias ll='eza -lFgShH --git --group-directories-first --time-style=long-iso'
#alias ls='ls -F --color=auto --group-directories-first'
alias ls='eza -1FhH --group-directories-first'

alias tree='eza --tree'

alias grep='grep --color=auto'
alias pip=pip3
alias python=python3
alias pudb=pudb3
alias mypy='mypy --strict --disallow-any-explicit'

# fc-list lists all fonts!
alias st="st -f 'DejaVu Sans Mono-15'"

# alias v='vimx'
# alias vi='vimx'
# alias vim='vimx'

# https://superuser.com/questions/1043806/how-to-exit-the-ranger-file-explorer-back-to-command-prompt-but-keep-the-current
# https://unix.stackexchange.com/questions/342064/ranger-cd-into-a-folder-and-invoke-shell/351592#351592
# https://github.com/ranger/ranger/issues/960
alias ranger='source ranger'
alias r='source ranger'

# default text editor of vimx (for ranger mostly)
VISUAL=vis; export VISUAL
EDITOR=vis; export EDITOR

PS1='\[\e]0;\u: \w\a\]\[\033[00;32m\]\u@\h\[\033[00m\]:\[\033[00;34m\]\w\[\033[00m\n\$ '

set -o vi

# This does not work correctly?
#man() {
#	env LESS_TERMCAP_mb=$'\E[01;31m' \
#	LESS_TERMCAP_md=$'\E[01;38;5;74m' \
#	LESS_TERMCAP_me=$'\E[0m' \
#	LESS_TERMCAP_se=$'\E[0m' \
#	LESS_TERMCAP_so=$'\E[38;5;246m' \
#	LESS_TERMCAP_ue=$'\E[0m' \
#	LESS_TERMCAP_us=$'\E[04;38;5;146m' \
#	man "$@"
#}

# for gpg-agent pinentry-tty and pinentry-curses
GPG_TTY=$(tty); export GPG_TTY

shopt -s globstar
