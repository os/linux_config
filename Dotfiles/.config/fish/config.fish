fish_vi_key_bindings

function ranger-cd                                                               
  set tempfile '/tmp/chosendir'                                                  
  /usr/bin/ranger --choosedir=$tempfile (pwd)                                    
  if test -f $tempfile                                                           
      if [ (cat $tempfile) != (pwd) ]                                            
         cd (cat $tempfile)                                                       
      end                                                                        
  end                                                                            
  rm -f $tempfile                                                                
end 

# abbr --add v vimx
# abbr --add vi vimx
# abbr --add vim vimx
abbr r ranger-cd
# abbr grep grep --color=auto --ignore-case --recursive
abbr grep "rg --ignore-case"
abbr pip pip3
abbr mypy "mypy --strict --disallow-any-explicit **/*.py"
abbr gl "git log --all --graph --show-signature"
abbr tree eza --tree
abbr cat bat
abbr ip ip -color=always
abbr diff delta

alias l. 'eza -1d1hH -F=always .* --group-directories-first'
alias ll 'eza -lgShH -F=always --git --group-directories-first --time-style=long-iso'
alias ls 'eza -1hH -F=always --group-directories-first'

set --export VISUAL vis
set --export EDITOR vis

# for gpg-agent pinentry-tty and pinentry-curses
set --global --export GPG_TTY (tty)

# Don't shorten the working directory in the prompt
set -U fish_prompt_pwd_dir_length 0

# set paths (this will be ignored all but first time, unless you delete fish_vars)
fish_add_path --prepend ~/bin/ ~/.local/bin/ ~/.cargo/bin/

# default nothing for group and other.
umask u=rwx,g-rwx,o-rwx
# umask u=rwx,g=,o=
