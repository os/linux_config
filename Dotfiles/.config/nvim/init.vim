set runtimepath^=~/.vim runtimepath+=~/.vim/after
let &packpath=&runtimepath
source ~/.vimrc

" Why does this get ignored??
lua <<EOF
vim.diagnostic.config({ virtual_text = false, })
EOF
