hi clear
syntax reset
let g:colors_name = "mono"
set background=dark
set t_Co=256
hi Normal guifg=#9b9b9b ctermbg=NONE guibg=#4a4a4a gui=NONE

hi DiffText guifg=#000000 guibg=NONE
hi ErrorMsg guifg=#000000 guibg=NONE
hi WarningMsg guifg=#000000 guibg=NONE
hi PreProc guifg=#000000 guibg=NONE
hi Exception guifg=#000000 guibg=NONE
hi Error guifg=#000000 guibg=NONE
hi DiffDelete guifg=#000000 guibg=NONE
hi GitGutterDelete guifg=#000000 guibg=NONE
hi GitGutterChangeDelete guifg=#000000 guibg=NONE
hi cssIdentifier guifg=#000000 guibg=NONE
hi cssImportant guifg=#000000 guibg=NONE
hi Type guifg=#000000 guibg=NONE
hi Identifier guifg=#000000 guibg=NONE
hi PMenuSel guifg=#ffffff guibg=NONE
hi Constant guifg=#ffffff guibg=NONE
hi Repeat guifg=#ffffff guibg=NONE
hi DiffAdd guifg=#ffffff guibg=NONE
hi GitGutterAdd guifg=#ffffff guibg=NONE
hi cssIncludeKeyword guifg=#ffffff guibg=NONE
hi Keyword guifg=#ffffff guibg=NONE
hi IncSearch guifg=#585858 guibg=NONE
hi Title guifg=#585858 guibg=NONE
hi PreCondit guifg=#585858 guibg=NONE
hi Debug guifg=#585858 guibg=NONE
hi SpecialChar guifg=#585858 guibg=NONE
hi Conditional guifg=#585858 guibg=NONE
hi Todo guifg=#585858 guibg=NONE
hi Special guifg=#585858 guibg=NONE
hi Label guifg=#585858 guibg=NONE
hi Delimiter guifg=#585858 guibg=NONE
hi Number guifg=#585858 guibg=NONE
hi CursorLineNR guifg=#585858 guibg=NONE
hi Define guifg=#585858 guibg=NONE
hi MoreMsg guifg=#585858 guibg=NONE
hi Tag guifg=#585858 guibg=NONE
hi String guifg=#585858 guibg=NONE
hi MatchParen guifg=#585858 guibg=NONE
hi Macro guifg=#585858 guibg=NONE
hi DiffChange guifg=#585858 guibg=NONE
hi GitGutterChange guifg=#585858 guibg=NONE
hi cssColor guifg=#585858 guibg=NONE
hi Function guifg=#9b9b9b guibg=NONE
hi Directory guifg=#cacaca guibg=NONE
hi markdownLinkText guifg=#cacaca guibg=NONE
hi javaScriptBoolean guifg=#cacaca guibg=NONE
hi Include guifg=#cacaca guibg=NONE
hi Storage guifg=#cacaca guibg=NONE
hi cssClassName guifg=#cacaca guibg=NONE
hi cssClassNameDot guifg=#cacaca guibg=NONE
hi Statement guifg=#ffffff guibg=NONE
hi Operator guifg=#ffffff guibg=NONE
hi cssAttr guifg=#ffffff guibg=NONE


hi Pmenu guifg=#9b9b9b guibg=#454545
hi SignColumn guibg=#4a4a4a
hi Title guifg=#9b9b9b
hi LineNr guifg=#353535 guibg=#4a4a4a
hi NonText guifg=#505050 guibg=#4a4a4a
hi Comment guifg=#505050 gui=italic
hi SpecialComment guifg=#505050 gui=italic guibg=NONE
hi CursorLine guibg=#454545
hi TabLineFill gui=NONE guibg=#454545
hi TabLine guifg=#353535 guibg=#454545 gui=NONE
hi StatusLine gui=bold guibg=#454545 guifg=#9b9b9b
hi StatusLineNC gui=NONE guibg=#4a4a4a guifg=#9b9b9b
hi Search guibg=#505050 guifg=#9b9b9b
hi VertSplit gui=NONE guifg=#454545 guibg=NONE
hi Visual gui=NONE guibg=#454545
