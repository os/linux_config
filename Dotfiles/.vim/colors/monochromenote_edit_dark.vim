" vim:set ts=8 sts=2 sw=2 tw=0 et:
"
" Vim color scheme: monochromenote
"
" Maintainer: MURAOKA Taro <koron.kaoriya@gmail.com>
" Repository: https://github.com/koron/vim-monochromenote
" https://robotmoon.com/256-colors/
" https://upload.wikimedia.org/wikipedia/commons/1/15/Xterm_256color_chart.svg
" 232-255 are grey, dark to light

set background=light
highlight clear
if exists("syntax_on")
  syntax reset
endif
let g:colors_name = "monochromenote"

" Normal group at first
highlight Normal ctermfg=White ctermbg=243 guifg=grey25 guibg=grey70

" default groups
highlight ColorColumn term=NONE cterm=NONE ctermfg=White ctermbg=White gui=NONE guifg=grey25 guibg=grey80
highlight Conceal ctermfg=240 ctermbg=243 guifg=grey90 guibg=grey70
highlight Cursor term=reverse cterm=reverse ctermfg=White ctermbg=243 gui=reverse guifg=grey25 guibg=grey70
highlight CursorColumn term=NONE cterm=NONE ctermfg=White ctermbg=White gui=NONE guifg=grey25 guibg=grey80
highlight CursorIM term=reverse cterm=reverse ctermfg=240 ctermbg=DarkGrey gui=reverse guifg=grey100 guibg=grey25
highlight CursorLine term=NONE cterm=NONE ctermfg=White ctermbg=White gui=NONE guifg=grey25 guibg=grey80
highlight CursorLineNr term=bold cterm=bold ctermfg=240 ctermbg=243 gui=bold guifg=grey90 guibg=grey70
highlight DiffAdd ctermfg=White ctermbg=White guifg=grey25 guibg=grey80
highlight DiffChange ctermfg=240 ctermbg=243 guifg=grey90 guibg=grey60
highlight DiffDelete ctermfg=240 ctermbg=White guifg=grey90 guibg=grey80
highlight DiffText ctermfg=White ctermbg=243 guifg=grey25 guibg=grey60
highlight Directory ctermfg=Black ctermbg=243 guifg=grey10 guibg=grey70
highlight ErrorMsg ctermfg=240 ctermbg=Black guifg=grey90 guibg=grey10
highlight FoldColumn ctermfg=White ctermbg=DarkGrey guifg=grey25 guibg=grey50
highlight Folded ctermfg=240 ctermbg=243 guifg=grey90 guibg=grey70
highlight LineNr ctermfg=240 ctermbg=243 guifg=grey90 guibg=grey70
highlight MatchParen ctermfg=White ctermbg=White guifg=grey25 guibg=grey90
highlight ModeMsg ctermfg=Black ctermbg=243 guifg=grey10 guibg=grey70
highlight MoreMsg ctermfg=Black ctermbg=243 guifg=grey10 guibg=grey70
highlight NonText term=bold cterm=bold ctermfg=240 ctermbg=243 gui=bold guifg=grey90 guibg=grey60
highlight Pmenu ctermfg=White ctermbg=243 guifg=grey25 guibg=grey60
highlight PmenuSbar ctermbg=DarkGrey guibg=grey40
highlight PmenuSel ctermfg=240 ctermbg=243 guifg=grey90 guibg=grey60
highlight PmenuThumb ctermbg=White guibg=grey90
highlight Question ctermfg=240 ctermbg=243 guifg=grey90 guibg=grey70
highlight Search ctermfg=240 ctermbg=DarkGrey guifg=grey80 guibg=grey50
highlight SignColumn ctermfg=White ctermbg=DarkGrey guifg=grey25 guibg=grey50
highlight SpecialKey ctermfg=240 ctermbg=243 guifg=grey90 guibg=grey70
highlight SpellBad term=undercurl cterm=undercurl ctermfg=White gui=undercurl guifg=grey25 guisp=grey90
highlight SpellCap term=undercurl cterm=undercurl ctermfg=White gui=undercurl guifg=grey25 guisp=grey25
highlight SpellLocal term=undercurl cterm=undercurl ctermfg=240 gui=undercurl guifg=grey90 guisp=grey25
highlight SpellRare term=undercurl cterm=undercurl ctermfg=240 gui=undercurl guifg=grey90 guisp=grey90
highlight StatusLine term=bold cterm=bold ctermfg=240 ctermbg=DarkGrey gui=bold guifg=grey90 guibg=grey25
highlight StatusLineNC term=NONE cterm=NONE ctermfg=LightGrey ctermbg=DarkGrey gui=NONE guifg=grey70 guibg=grey25
highlight StatusLineTerm term=bold cterm=bold ctermfg=240 ctermbg=DarkGrey gui=bold guifg=grey90 guibg=grey40
highlight StatusLineTermNC term=NONE cterm=NONE ctermfg=LightGrey ctermbg=DarkGrey gui=NONE guifg=grey70 guibg=grey40
highlight TabLine ctermfg=LightGrey ctermbg=Black guifg=grey60 guibg=grey10
highlight TabLineFill ctermbg=Black guibg=grey10
highlight TabLineSel term=bold cterm=bold ctermfg=White ctermbg=243 gui=bold guifg=grey25 guibg=grey60
highlight Terminal ctermfg=LightGrey ctermbg=DarkGrey guifg=grey70 guibg=grey25
highlight Title term=bold cterm=bold ctermfg=240 ctermbg=243 gui=bold guifg=grey90 guibg=grey70
highlight VertSplit term=NONE cterm=NONE ctermfg=White ctermbg=DarkGrey gui=NONE guifg=grey25 guibg=grey25
highlight Visual term=reverse cterm=reverse guibg=grey85
highlight VisualNOS term=bold,underline cterm=bold,underline gui=bold,underline guibg=grey85
highlight WarningMsg term=bold cterm=bold ctermfg=240 ctermbg=DarkGrey gui=bold guifg=grey90 guibg=grey25
highlight WildMenu term=reverse cterm=reverse ctermfg=240 ctermbg=DarkGrey gui=reverse guifg=grey100 guibg=grey25
highlight lCursor term=reverse cterm=reverse ctermfg=240 ctermbg=DarkGrey gui=reverse guifg=grey100 guibg=grey25

" custom groups
highlight Comment ctermfg=240 ctermbg=243 guifg=grey90 guibg=grey70
highlight Statement term=bold cterm=bold ctermfg=Black ctermbg=243 gui=bold guifg=grey25 guibg=grey70
highlight Type ctermfg=Black ctermbg=243 guifg=grey10 guibg=grey70
highlight PreProc ctermfg=Black ctermbg=243 guifg=grey10 guibg=grey70
highlight Identifier ctermfg=Black ctermbg=243 guifg=grey10 guibg=grey70
highlight Special ctermfg=Black ctermbg=243 guifg=grey10 guibg=grey80
highlight Constant ctermfg=240 ctermbg=243 guifg=grey25 guibg=grey80
highlight Error ctermfg=White ctermbg=White guifg=grey50 guibg=grey100
highlight Underlined term=underline cterm=underline ctermfg=White ctermbg=243 gui=underline guifg=grey25 guibg=grey70
highlight Todo ctermfg=240 ctermbg=White guifg=grey100 guibg=grey80

" links
highlight link IncSearch Cursor

" WARNING: undefined default groups:
"  - EndOfBuffer
"  - LineNrAbove
"  - LineNrBelow
"  - QuickFixLine
