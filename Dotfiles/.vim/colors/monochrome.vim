" monochrome.vim
"
" Original Author: Xavier Noria <fxn@hashref.com>
"         License: MIT
"     Modified By: Jake Bauer <jbauer@paritybit.ca>

set background=dark

hi clear
if exists('syntax_on')
	syntax reset
endif

let g:colors_name = 'monochrome'

let s:black      = ['#080808', 232]
let s:red        = ['#D75F5F', 167]
let s:green      = ['#5FAF5F', 71]
let s:blue       = ['#0087FF', 33]
let s:yellow     = ['#D7D700', 184]
let s:white      = ['#FFFFFF', 231]
let s:accent     = ['#5F5F5F', 59]
let s:cursorline = ['#4E4E4E', 239]
let s:comment    = ['#6C6C6C', 242]
let s:faded      = ['#444444', 238]

let s:default_fg = s:white
let s:default_bg = s:black

let s:italic    = 'italic'
let s:bold      = 'bold'
let s:underline = 'underline'
let s:none      = 'NONE'

let s:default_lst = []
let s:default_str = ''

let s:comment_attr = s:italic

function! s:hi(...)
	let group = a:1
	let fg    = get(a:, 2, s:default_fg)
	let bg    = get(a:, 3, s:default_bg)
	let attr  = get(a:, 4, s:default_str)

	let cmd = ['hi', group]

	if fg != s:default_lst
		call add(cmd, 'guifg='.fg[0])
		call add(cmd, 'ctermfg='.fg[1])
	endif

	if bg != s:default_lst
		call add(cmd, 'guibg='.bg[0])
		call add(cmd, 'ctermbg='.bg[1])
	endif

	if attr != s:default_str
		call add(cmd, 'gui='.attr)
		call add(cmd, 'cterm='.attr)
	endif

	exec "hi clear " group
	exec join(cmd, ' ')
endfunction


" --- Vim interface ------------------------------------------------------------
call s:hi('Normal', s:white, s:black)
call s:hi('Cursor', s:black, s:white)
call s:hi('CursorLine', s:white, s:cursorline, s:none)
call s:hi('CursorLineNr', s:white, s:default_bg, s:bold)
call s:hi('ColorColumn', s:white, s:faded)
call s:hi('Search', s:white, s:accent)
call s:hi('Visual', s:white, s:accent)
call s:hi('ErrorMsg', s:red, s:default_bg)

" Status bar
call s:hi('StatusLine', s:white, s:faded)
call s:hi('StatusLineNC', s:white, s:faded)

" Tabs
call s:hi('TabLine', s:white, s:faded)
call s:hi('TabLineSel', s:white, s:accent, s:bold)
call s:hi('TabLineFill', s:white, s:black)

" Tildes at the bottom of a buffer, etc.
call s:hi('NonText', s:faded)

" Folding.
call s:hi('FoldColumn', s:faded)
call s:hi('Folded')

" Line numbers gutter.
call s:hi('LineNr', s:accent)

" Small arrow used for tabs.
call s:hi('SpecialKey', s:accent, s:default_bg, s:bold)

" File browsers.
call s:hi('Directory', s:white, s:default_bg, s:bold)

" Help.
call s:hi('helpSpecial')
call s:hi('helpHyperTextJump', s:accent, s:default_bg, s:underline)
call s:hi('helpNote')

" Popup menu.
call s:hi('Pmenu', s:white, s:accent)
call s:hi('PmenuSel', s:accent, s:white)

" Notes.
call s:hi('Todo', s:white, s:default_bg, s:bold)

" Signs.
call s:hi('SignColumn')

" --- Diff Mode ----------------------------------------------------------------
call s:hi('DiffAdd', s:green, s:default_bg)
call s:hi('DiffDelete', s:red, s:default_bg)
call s:hi('DiffChange', s:white, s:default_bg)
call s:hi('DiffText', s:yellow, s:default_bg)

" --- Programming languages ----------------------------------------------------
call s:hi('Statement', s:white, s:default_bg, s:bold)
call s:hi('PreProc', s:white, s:default_bg, s:bold)
call s:hi('String', s:accent)
call s:hi('Comment', s:comment, s:default_bg, s:comment_attr)
call s:hi('Constant', s:accent, s:default_bg, s:bold)
call s:hi('Number', s:accent)
call s:hi('Type', s:white, s:default_bg, s:bold)
call s:hi('Function', s:white)
call s:hi('Identifier', s:default_fg, s:default_bg, s:none)
call s:hi('Special')
call s:hi('MatchParen', s:white, s:black, s:underline)
call s:hi('Error', s:red, s:black, s:bold)

" --- VimL ---------------------------------------------------------------------
call s:hi('vimOption')
call s:hi('vimGroup')
call s:hi('vimHiClear')
call s:hi('vimHiGroup')
call s:hi('vimHiAttrib')
call s:hi('vimHiGui')
call s:hi('vimHiGuiFgBg')
call s:hi('vimHiCTerm')
call s:hi('vimHiCTermFgBg')
call s:hi('vimSynType')
hi link vimCommentTitle Comment

" --- Ruby ---------------------------------------------------------------------
call s:hi('rubyConstant')
call s:hi('rubySharpBang', s:comment)
call s:hi('rubyStringDelimiter', s:accent)
call s:hi('rubyStringEscape', s:accent)
call s:hi('rubyRegexpEscape', s:accent)
call s:hi('rubyRegexpAnchor', s:accent)
call s:hi('rubyRegexpSpecial', s:accent)

" --- Elixir -------------------------------------------------------------------
call s:hi('elixirAlias', s:default_fg, s:default_bg, s:none)
call s:hi('elixirDelimiter', s:accent)
call s:hi('elixirSelf', s:default_fg, s:default_bg, s:none)

" For ||, ->, etc.
call s:hi('elixirOperator')

" Module attributes like @doc or @type.
hi link elixirVariable Statement

" While rendered as comments in other languages, docstrings are strings,
" experimental.
hi link elixirDocString String
hi link elixirDocTest String
hi link elixirStringDelimiter String

" --- Perl ---------------------------------------------------------------------
call s:hi('perlSharpBang', s:comment)
call s:hi('perlStringStartEnd', s:accent)
call s:hi('perlStringEscape', s:accent)
call s:hi('perlMatchStartEnd', s:accent)

" --- Python -------------------------------------------------------------------
call s:hi('pythonEscape', s:accent)

" --- JavaScript ---------------------------------------------------------------
call s:hi('javaScriptFunction', s:white, s:default_bg, s:bold)

" --- Diffs --------------------------------------------------------------------
call s:hi('diffFile', s:faded)
call s:hi('diffNewFile', s:faded)
call s:hi('diffIndexLine', s:comment)
call s:hi('diffLine', s:yellow)
call s:hi('diffSubname', s:faded)
call s:hi('diffAdded', s:green)
call s:hi('diffRemoved', s:red)

" --- Markdown -----------------------------------------------------------------
call s:hi('Title', s:white, s:default_bg, s:bold)
call s:hi('markdownHeadingDelimiter', s:white, s:default_bg, s:bold)
call s:hi('markdownHeadingRule', s:white, s:default_bg, s:bold)
call s:hi('markdownLinkText', s:blue, s:default_bg, s:underline)

" --- HTML ---------------------------------------------------------------------
call s:hi('htmlLink', s:blue, s:default_bg, s:underline)
call s:hi('htmlArg', s:default_fg, s:default_bg)

" --- Gemini -------------------------------------------------------------------
" Based on this plugin: https://tildegit.org/sloum/gemini-vim-syntax
call s:hi('gmiHeader', s:white, s:default_bg, s:bold)
call s:hi('gmiLinkStart', s:white, s:default_bg, s:bold)
call s:hi('gmiLinkURL', s:blue, s:default_bg, s:underline)
call s:hi('gmiLinkTitle', s:accent, s:default_bg, s:none)
call s:hi('gmiMono', s:accent, s:default_bg, s:bold)
call s:hi('gmiQuoteLine', s:accent, s:default_bg, s:bold)

" --- Spelling -----------------------------------------------------------------
call s:hi('SpellBad', s:red, s:default_bg, s:bold, s:underline)
call s:hi('SpellCap', s:white, s:default_bg, s:bold)
call s:hi('SpellLocal', s:default_fg, s:default_bg, s:none)
call s:hi('SpellRare', s:default_fg, s:default_bg, s:none)
